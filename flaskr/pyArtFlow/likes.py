from py2neo import Relationship

from .config import ArtFlow
from .post_actions import load_postNode
from .user_actions import load_userNode


def like(user_id=None, node=None):
    user = load_userNode(user_id)
    ArtFlow.create(Relationship(user, "LIKES", node))


def dislike(user_id=None, node=None):
    user = load_userNode(user_id)
    r_like = ArtFlow.match(nodes={user, node}, r_type='LIKES').first()
    ArtFlow.separate(r_like)


def likes_count(node=None):
    return ArtFlow.match(nodes={node}, r_type="LIKES").count()


def likers(node=None):
    for r_like in ArtFlow.match(nodes={node}, r_type="LIKES"):
        yield r_like.nodes[0]

