from py2neo import Graph, Schema, Node, Relationship, Subgraph
from datetime import datetime
from .config import ArtFlow


def load_friends(user_id):
    return ArtFlow.run(
        f"""
            MATCH (m:User {{email:"{str(user_id)}"}})-[:`IS_FRIEND_WITH`]->(f:User) 
            RETURN f
        """
    )


def load_user(email=None, id=None):
    return ArtFlow.nodes.match("User", email=email, identity_token=id).first()

def create_user(user=None):
    ArtFlow.create(user)

def load_userNode(email=None):
    return ArtFlow.nodes.match("User", email=email).first()

if __name__ == "__main__":
    for friend in friends("karlschuller@yahoo.fr"):
        print(friend.get('f')['email'])
