try:
    from .comment import *
    from .post_actions import load_postNode, get_all_user_posts
    from .user_actions import load_userNode
    from .config import ArtFlow
except:
    from flaskr.pyArtFlow.comment import *
    from flaskr.pyArtFlow.post_actions import load_postNode, get_all_user_posts
    from flaskr.pyArtFlow.user_actions import load_userNode
    from flaskr.pyArtFlow.config import ArtFlow



def load_commentNode(user_id=None, creation_datetime=None):
    return ArtFlow.nodes.match('Comment', sender_id=user_id, creation_datetime=creation_datetime).first()


def drop_contentsRelation(node):
    # Return the actual last comment on the post
    content = ArtFlow.match_one(nodes=set([node]), r_type="CONTENT")
    if content != None:
        ArtFlow.separate(content)
        return content.nodes[-1]
    else:
        return None


def push_contentsSubgraph(user, comment, post, last_comment=None):
    new_relations = [
        Relationship(user, "POSTS", comment),
        Relationship(post, "CONTENT", comment),
    ]
    if last_comment:
        new_relations.append(Relationship(comment, "FOLLOW", last_comment))
    ArtFlow.create(Subgraph(relationships=new_relations))


def load_user_comments(user_id=None):
    return ArtFlow.nodes.match('Post').where(f"'{user_id}' = _.sender_id")

def load_comments(previous=None, n=10):
    while n > 0:
        follows = ArtFlow.match_one([previous], 'FOLLOW')
        if follows is None:
            break
        previous = follows.end_node
        yield previous
        n -= 1



def load_post_comments(post_id=None, last_comment=None):
    post = load_postNode(post_id)
    start = ArtFlow.match_one([post], "CONTENT").end_node
    if last_comment and last_comment != start:
        load_post_comments(last_comment)
    else:
        last_comment = start
        comments = list(load_comments(last_comment))
        return (
            comment for comment in [last_comment] + comments
        )


def update_comment(sender_id=None, creation_datetime=None, content=None):
    comment = ArtFlow.nodes.match('Comment', sender_id=sender_id, creation_datetime=creation_datetime).first()
    comment.comment = content
    ArtFlow.push(comment)


def post_comment(user_id, post_id, *comment_content, **reference):
    user = load_userNode(user_id)
    post = load_postNode(post_id)
    last_comment = drop_contentsRelation(post)
    comment = CommentNode(comment=comment_content[0], sender_id=user['email'])
    push_contentsSubgraph(user=user, post=post, comment=comment, last_comment=last_comment)


if __name__ == "__main__":
    for comment in load_post_comments("1234567890_10216617594006061"):
        print(comment)
