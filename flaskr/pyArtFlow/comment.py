from py2neo import Graph, Schema, Node, Relationship, Subgraph
from datetime import datetime


class CommentSchema(Schema):
    def __init__(self, graph):
        super().__init__(graph)
        self.existence_constraint()
        self.comment_index()

    def existence_constraint(self):
        self.graph.run(
            """
                CREATE CONSTRAINT comment_constraint IF NOT EXISTS
                ON (c:Comment)
                ASSERT EXISTS (c.comment)
            """
        )

    def comment_index(self):
        self.graph.run(
            """
                CREATE INDEX comment_index IF NOT EXISTS
                FOR (c:Comment)
                ON (c.creation_datetime, c.sender_id)
            """
        )

class CommentNode(Node):
    def __init__(self, sender_id=None, comment=None, url="", **kwproperties):
        super().__init__("Comment")
        self.update(kwproperties)
        self.update({
            "comment": comment,
            "url": url,
            "sender_id": sender_id,
            "creation_datetime": str(datetime.today()),
        })

if __name__=="__main__":
    CommentSchema(Graph("bolt://neo4j:groupe_8@localhost:7687"))
