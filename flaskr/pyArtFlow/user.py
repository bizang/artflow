from py2neo import Graph, Schema, Node, Relationship, Subgraph
from datetime import datetime


class UserSchema(Schema):
    def __init__(self, graph):
        self.graph = graph
        self.unique_constraint()
        self.existence_constraint()

    def unique_constraint(self):
        self.graph.run(
            """
                CREATE CONSTRAINT unique_email IF NOT EXISTS
                ON (user:User)
                ASSERT user.email IS UNIQUE;
            """
        )
        self.graph.run(
            """
                CREATE CONSTRAINT unique_phoneNo IF NOT EXISTS
                ON (user:User)
                ASSERT user.phoneNo IS UNIQUE
            """
        )

    def key(self):
        self.graph.run(
            """
                CREATE CONSTRAINT user_key IF NOT EXISTS
                ON (user:User)
                ASSERT (user.email, user.identity_token) IS NODE KEY
            """
        )

    def existence_constraint(self):
        self.graph.run(
            """
                CREATE CONSTRAINT firstName_constraint IF NOT EXISTS
                ON (user:User)
                ASSERT EXISTS (user.first_name) 
            """
        )
        self.graph.run(
            """
                CREATE CONSTRAINT lastName_constraint IF NOT EXISTS
                ON (user:User)
                ASSERT EXISTS (user.last_name)
            """
        )
        self.graph.run(
            """
                CREATE CONSTRAINT phoneNo_constraint IF NOT EXISTS
                ON (user:User)
                ASSERT EXISTS (user.phoneNo)
            """
        )
        self.graph.run(
            """
                CREATE CONSTRAINT birthday_constraint IF NOT EXISTS
                ON (user:User)
                ASSERT EXISTS (user.birthday)
            """
        )
        self.graph.run(
            """
                CREATE CONSTRAINT country_constraint IF NOT EXISTS
                ON (user:User)
                ASSERT EXISTS (user.country)
            """
        )
        self.graph.run(
            """
                CREATE CONSTRAINT city_constraint IF NOT EXISTS
                ON (user:User)
                ASSERT EXISTS (user.city)
            """
        )


class UserNode(Node):
    def __init__(self, **identities):
        super().__init__("User")
        self.update(identities)
        self.update({"user_token": "2cec711d-ca14-"})
        self.update({"creation_datetime": str(datetime.today())})
        self.update({"last_login": str(datetime.today())})
        self.update({"num_logins": 11})


class RelationshipFriends(Relationship):
    def __init__(self, friend1=None, friend2=None):
        super().__init__(friend1, "IS_FRIEND_WITH", friend2, since=2021)

if __name__=="__main__":
    UserSchema(Graph("bolt://neo4j:groupe_8@localhost:7687"))