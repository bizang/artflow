from ..inbox_actions import *
from ..test_backend.test_user import user_template
from ..user_actions import load_userNode

base_message = "Bonjour"

if __name__ == '__main__':
    user_id = user_template["email"]
    user = load_userNode(user_id)
    friends = [relation.nodes[-1] for relation in ArtFlow.match([user], "En attente")]

    for friend in friends:
        for i in range(10):
            if i % 2 == 0:
                send_message(user_id, friend["email"], f"{i + 1} - Bonjour {friend['first_name']}")
            else:
                send_message(friend["email"], user_id, f"{i + 1} - Bonjour {user['first_name']}")
