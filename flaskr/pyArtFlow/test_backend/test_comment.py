from ..comment import CommentNode
from ..comment_actions import push_contentsSubgraph, drop_contentsRelation
from ..post_actions import get_all_user_posts
from ..user_actions import load_userNode

if __name__ == "__main__":
    user = load_userNode("karlschuller@yahoo.fr")
    post = get_all_user_posts(user['email']).next().get('p')
    comment = CommentNode(sender_id=user['email'], comment="Ceci est mon deuxième commentaire")
    last_comment = CommentNode(sender_id=user['email'], comment="Ceci est mon tout premier commentaire")
    push_contentsSubgraph(user=user, post=post, comment=comment, last_comment=last_comment)
    last_comment = drop_contentsRelation(post)
    new_comment = CommentNode(sender_id=user['email'], comment="Dernier commentaire")
    push_contentsSubgraph(user=user, post=post, comment=new_comment, last_comment=last_comment)