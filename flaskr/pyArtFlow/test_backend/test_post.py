from py2neo import Graph
from ..post import *
from ..inbox_actions import load_userNode
from ..test_backend.test_user import user_template
from ..config import ArtFlow
from ..post_actions import *



post_template = NodeUserPost(
    id="1234567890_10216617594006061",
    url="https://www.facebook.com/1234567890/posts/10216617594006061",
    type="photo", action="added_photo",
    text="Hello world!",
    date_created="Mon, 24 Dec 2018 14:30:19 0100",
    date_updated="Tue, 25 Dec 2018 20:14:59 0100",
    media="https://scontent.xx.fbcdn.net/v/t1.0-9/45454545_10216617616806631_3315147954267881472_n.jpg?_nc_cat=101&_nc_ht=scontent.xx&oh=7fbf86081aedca60dffc9f2ae6dc1e71&oe=5CFC5B02")
link_template = NodeAttachedLink(link="https://www.facebook.com/1234567890/posts/10216617594006061")
place_template = NodePlace(name="La Bella Vista Ristorante")
location_template = NodeLocation(city="New York", country="USA", street="163 Mulberry St", zip="NY 10013", latitude= 40.719981, longitude= -73.99)

if __name__ == '__main__':
    email = user_template["email"]
    user = load_userNode(email)
    post = post_template
    link = link_template
    place = place_template
    location = location_template

    ArtFlow.create(post)
    subgraphUserPost = SubgraphUserPost(user=user, post=post, link=link, place=place, location=location)
    ArtFlow.create(subgraphUserPost)

