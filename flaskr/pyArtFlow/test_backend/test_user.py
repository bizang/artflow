import prenoms
from ..user import *
from random import choice, randint
from ..config import ArtFlow


user_template = UserNode(
    identity_token="4d6c-a7f6-950b4c47938f",
    provider="twitter",
    id="http://twitter.com/ExampleUser",
    first_name="Karl",
    last_name="Schuller",
    email="karlschuller@yahoo.fr",
    phoneNo="0653689849",
    country="France",
    city="Nice",
    age=26,
    birthday="12/03/1995",
    user_status=" ",
    user_level=" ",
    site_web=" "
)


def random_phone_number():
    number = '0'
    for _ in range(9):
        number += f'{randint(0, 10)}'
    return number


def email(prenom=None, nom=None):
    domain = ["gmail.fr", "yahoo.fr", "hotmail.fr", "gmail.com", "yahoo.com", "hotmail.com"]
    return prenom + nom + "@" + choice(domain)


def random_user(user=user_template.copy()):
    user["first_name"] = prenoms.get_prenom()
    user["last_name"] = prenoms.get_nom()
    user["email"] = email(user["first_name"], user["last_name"])
    user["identity_token"] = str(randint(1, 10 ** 5))
    user["phoneNo"] = str(random_phone_number())

    return UserNode(**dict(user.items()))


if __name__ == '__main__':
    ArtFlow.delete_all()
    user = user_template
    friends = [random_user() for _ in range(10)]

    #    Add users and relationships
    ArtFlow.create(user)
    for friend in friends:
        ArtFlow.create(friend)
        ArtFlow.create(RelationshipFriends(user, friend))
        ArtFlow.create(RelationshipFriends(friend, user))
