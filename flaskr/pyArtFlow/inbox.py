from py2neo import Graph, Schema, Node, Relationship, Subgraph
from datetime import datetime


class MessageSchema(Schema):
    def __init__(self, graph):
        super().__init__(graph)
        self.existence_constraint()
        self.message_index()
        self.conversation_index()

    def existence_constraint(self):
        self.graph.run(
            """
                CREATE CONSTRAINT message_constraint IF NOT EXISTS
                ON (m:Message)
                ASSERT EXISTS (m.message)
            """
        )

    def message_index(self):
        self.graph.run(
            """
                CREATE INDEX message_index IF NOT EXISTS
                FOR (m:Message)
                ON (m.creation_datetime, m.sender_id)
            """
        )

    def conversation_index(self):
        try:
            self.graph.run(
                """
                    CALL db.index.fulltext.createNodeIndex(
                            "usersAndCreation_datetime", 
                            ["Message"],
                            ["users_id", "creation_datetime"]
                        )
                """
            )
        except:
            pass


class MessageNode(Node):
    def __init__(self, sender_id=None, message=None, url="", **kwproperties):
        super().__init__("Message")
        self.update(kwproperties)
        self.update({
            "message": message,
            "url": url,
            "sender_id": sender_id,
            "creation_datetime": str(datetime.today()),
            "mode": 'rr'
        })


class ConversationNode(Node):
    def __init__(self, *users_id):
        super().__init__("Conversation")
        self.update({
            "users_id": users_id,
            "creation_datetime": str(datetime.today()),
            "last_update": str(datetime.today())
        })


class Conversation(Subgraph):
    def __init__(self, sender=None, receiver=None, last_message=None, conversation_node=None, reference=None):
        self.sender = sender
        self.receiver = receiver
        self.last_message = last_message
        self.reference = reference
        if not conversation_node:
            conversation_node = ConversationNode(sender["email"], receiver["email"])
        self.conversation_node = conversation_node
        self.creation_datetime = self.conversation_node["creation_datetime"]
        relationships = [
            Relationship(self.sender, "EDITS", self.conversation_node, at=str(datetime.today()), unread=0),
            Relationship(self.receiver, "EDITS", self.conversation_node, at=str(datetime.today()), unread=0),
            Relationship(self.sender, "SENDS", self.last_message),
            Relationship(self.conversation_node, "CONTENTS", self.last_message)
        ]
        if reference:
            relationships.append(
                Relationship(self.last_message, "IS_REFERED_TO", self.reference)
            )
        super().__init__(relationships=relationships)


if __name__ == "__main__":
    MessageSchema(Graph("bolt://neo4j:groupe_8@localhost:7687"))
