from py2neo import Node, Schema, Subgraph, Relationship, Graph


class PostSchema(Schema):
    def __init__(self, graph):
        self.graph = graph
        self.unique_constraint()
        self.existence_constraint()

    def unique_constraint(self):
        self.graph.run(
            """
                CREATE CONSTRAINT post_key IF NOT EXISTS
                ON (post:Post)
                ASSERT post.id IS UNIQUE;
            """
        )
        self.graph.run(
            """
                CREATE CONSTRAINT unique_url IF NOT EXISTS
                ON (post:Post)
                ASSERT post.url IS UNIQUE
            """
        )

    def existence_constraint(self):
        self.graph.run(
            """
                CREATE CONSTRAINT url_constraint IF NOT EXISTS
                ON (link:Link)
                ASSERT EXISTS (link.link)
            """
        )
        self.graph.run(
            """
                CREATE CONSTRAINT place_constraint IF NOT EXISTS
                ON (place:Place)
                ASSERT EXISTS (place.name) 
            """
        )
        self.graph.run(
            """
                CREATE CONSTRAINT city_constraint IF NOT EXISTS
                ON (location:Location)
                ASSERT EXISTS (location.city) 
            """
        )
        self.graph.run(
            """
                CREATE CONSTRAINT country_constraint IF NOT EXISTS
                ON (location:Location)
                ASSERT EXISTS (location.country) 
            """
        )
        self.graph.run(
            """
                CREATE CONSTRAINT street_constraint IF NOT EXISTS
                ON (location:Location)
                ASSERT EXISTS (location.street) 
            """
        )
        self.graph.run(
            """
                CREATE CONSTRAINT zip_constraint IF NOT EXISTS
                ON (location:Location)
                ASSERT EXISTS (location.zip) 
            """
        )
        self.graph.run(
            """
                CREATE CONSTRAINT latitude_constraint IF NOT EXISTS
                ON (location:Location)
                ASSERT EXISTS (location.latitude) 
            """
        )
        self.graph.run(
            """
                CREATE CONSTRAINT longitude_constraint IF NOT EXISTS
                ON (location:Location)
                ASSERT EXISTS (location.longitude) 
            """
        )

class NodeUserPost(Node):
    def __init__(self, **properties):
        super().__init__("Post")
        self.update(properties)


class NodeAttachedLink(Node):
    def __init__(self, **properties):
        super().__init__("Link")
        self.update(properties)


class NodePlace(Node):
    def __init__(self, **properties):
        super().__init__("Place")
        self.update(properties)


class NodeLocation(Node):
    def __init__(self, **properties):
        super().__init__("Location")
        self.update(properties)


class SubgraphUserPost(Subgraph):
    def __init__(self, user=None, post=None, link=None, place=None, location=None):
        nodes = [post, link, place, location]
        relationships = [
            Relationship(user, "PUBLISHES", post),
            Relationship(post, "HAS_LINK", link),
            Relationship(place, "HAS_LOCATION", location),
            Relationship(post, "HAS_PLACE", place)
        ]
        super().__init__(relationships=relationships)


class NodeUserMedia(Node):
    def __init__(self, **properties):
        super().__init__("Media")
        self.update(properties)


class SubgraphUserMedia(Subgraph):
    def __init__(self, post=None, media=None):
        nodes = [post, media]
        relationships = [
            Relationship(post, "HAS_MEDIA", media),
        ]
        super().__init__(relationships=relationships)

if __name__ == "__main__":
    PostSchema(Graph("bolt://neo4j:groupe_8@localhost:7687"))