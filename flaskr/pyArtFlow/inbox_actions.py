from py2neo import Graph, Schema, Node, Relationship, Subgraph
from datetime import datetime
from .inbox import *
from .config import ArtFlow
from .user_actions import load_userNode


def edit_conversation(friend_id, user_id):
    conversation = load_conversationNode([user_id, friend_id])
    user = load_userNode(user_id)
    edits = ArtFlow.relationships.match(nodes=set([user, conversation]), r_type="EDITS").first()
    edits["at"] = str(datetime.today())
    edits["unread"] = 0
    ArtFlow.push(edits)


def create_conversationNode(users_id=[]):
    sender = load_userNode(users_id[0])
    receiver = load_userNode(users_id[-1])
    message = MessageNode(message=" ", sender_id=sender['email'])
    conversation = Conversation(sender, receiver, message)
    ArtFlow.create(subgraph=conversation)


def load_conversationNode(users_id=[]):
    conversation = ArtFlow.nodes.match(
        "Conversation",
        users_id=users_id
    ).first()
    if conversation:
        return conversation
    else:
        conversation = ArtFlow.nodes.match(
            "Conversation",
            users_id=users_id[::-1]
        ).first()
        if conversation:
            return conversation
        else:
            create_conversationNode(users_id)
            return load_conversationNode(users_id)


def drop_contentsRelation(conversation):
    # Separate conversation and the last message of the conversation
    # Return the actual last message of the conversation
    content = ArtFlow.match_one(nodes=set([conversation]), r_type="CONTENTS")
    ArtFlow.separate(content)
    return content.nodes[-1]


def push_messageSubgraph(user, message, conversation, last_message, reference=None):
    new_relations = [
        Relationship(user, "SENDS", message),
        Relationship(conversation, "CONTENTS", message),
        Relationship(message, "FOLLOWS", last_message)
    ]
    ArtFlow.create(Subgraph(relationships=new_relations))


def unread_incrementation(user, conversation):
    # update unread property of EDITS's relationships of these conversation, except on the sender
    edits = ArtFlow.match(nodes=set([conversation]), r_type="EDITS")
    for edit in edits:
        if user not in edit.nodes:
            edit["unread"] += 1
            ArtFlow.push(edit)


def send_message(user_id, receiver_id, *message_content, **reference):
    user = load_userNode(user_id)
    receiver = load_userNode(receiver_id)
    conversation = load_conversationNode([user_id, receiver_id])
    last_message = drop_contentsRelation(conversation)
    message = MessageNode(message=message_content[0], sender_id=user['email'])
    push_messageSubgraph(user, message, conversation, last_message)
    unread_incrementation(user, conversation)


# (_818:Message {creation_datetime: '2021-04-15 03:28:41.139744', message:['8 - Bonjour Karl'], mode: 'rr', url: ''})
# sender: 'Melissa'
def match_message(creation_datetime=None, sender_email=None):
    return ArtFlow.nodes.match("Message", creation_datetime=creation_datetime, sender_id=sender_email).first()


def delete_message(user=None, creation_datetime=None, sender_email=None):
    message = match_message(creation_datetime, sender_email)
    if user["email"] == sender_email:
        message['mode'] = '-' + message['mode'][1]
    else:
        message['mode'] = message['mode'][0] + '-'
    ArtFlow.push(Relationship(user, "SENDS", message))


def load_messages(previous=None, n=100):
    while n > 0 and previous['message'].strip():
        previous = ArtFlow.match_one([previous], 'FOLLOWS').end_node
        yield previous
        n -= 1


def load_conversation(sender_id=None, receiver_id=None, users_id=None, last_message=None):
    if users_id:
        sender_id, receiver_id = users_id
    if sender_id and receiver_id:
        users_id = [sender_id, receiver_id]
    conversationNode = load_conversationNode(users_id)
    start = ArtFlow.match_one([conversationNode], "CONTENTS").end_node
    if last_message and last_message != start:
        load_conversation(last_message)
    else:
        last_message = start
        messages = list(load_messages(last_message))
        return (
            msg for msg in [last_message] + messages
            if msg['message'].strip() and msg['mode'][sender_id != msg['sender_id']] != '-'
        )


def load_inbox(user_id=None):
    return ArtFlow.run(
        f"""
            MATCH (u:User)-[e:EDITS]->(c:Conversation)-[C:CONTENTS]->(m:Message) 
            WHERE ('{user_id}' in c.users_id) and ('{user_id}' <> u.email)
            return u, m, e.unread
            order by m.creation_datetime desc    
        """
    )


def datetime_format(my_datetime=None):
    dt = datetime.fromisoformat(my_datetime)
    if dt.date() == datetime.today().date():
        return f"{('0' + str(dt.hour))[-2:]}:{('0' + str(dt.minute))[-2:]}"
    elif dt.date().toordinal() == datetime.today().date().toordinal() - 1:
        return 'Hier'
    elif dt.date().toordinal() == datetime.today().date().toordinal() - 2:
        return 'Avant-hier'
    else:
        return str(dt.date())


def search_in_inbox(text=None, user_id=None):
    for conversation in ArtFlow.nodes.match('Conversation').where(f"'{user_id}' in _.users_id"):
        _ = search_in_conversation(text=text, users_id=conversation['users_id'])
        if _:
            for message in _:
                yield message


def search_in_conversation(text=None, users_id=None):
    for message in load_conversation(users_id=users_id):
        if text.lower() in str(message['message']).lower():
            yield message


if __name__ == "__main__":
    # email = ArtFlow.nodes.match("User").all()[3]["email"]
    # users_id = [email, "karlschuller@yahoo.fr"]
    # conversations = load_inbox("karlschuller@yahoo.fr")
    # for friend, message, unread in list(conversations):
    #     print(f"unread: {unread} {message['creation_datetime']} - {friend['first_name']} - {message['message']}")
    # conversation = load_conversation(users_id)
    # for message in conversation:
    #     print(f"{message['message']}")
    # edit_conversation("SafyaCarpin@gmail.com", "karlschuller@yahoo.fr")

    for message in search_in_inbox('2', "karlschuller@yahoo.fr"):
        print(message)
