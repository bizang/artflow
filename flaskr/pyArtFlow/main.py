from py2neo import Graph, Relationship
from inbox import *
from test_backend.test_user import user_template
from inbox_actions import *
from user import *
from .config import ArtFlow

if __name__ == '__main__':
    email = user_template["email"]
    user = load_userNode(email)
    friend = ArtFlow.match([user], "En attente").first().nodes[-1]
    conversation = load_conversationNode([user["email"], friend["email"]])
    last_message = ArtFlow.match_one([conversation], "CONTENTS").nodes[-1]

    def previews_message(last_message=None):
        return ArtFlow.match_one(nodes=[last_message], r_type="FOLLOWS").end_node

    messages = []
    previews = last_message
    i = 0
    while i < 10:
        messages.append(previews)
        previews = previews_message(previews)
        i += 1

    for message in messages:
        print(message)
