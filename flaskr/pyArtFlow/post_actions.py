from datetime import datetime
from .post import *
from .config import ArtFlow
from .user_actions import load_userNode


def create_post(user, post, link, place, location):
    subgraphUserPost = SubgraphUserPost(user=user, post=post, link=link, place=place, location=location)
    ArtFlow.create(subgraphUserPost)


def load_postNode(post_id=None):
    return ArtFlow.nodes.match('Post', id=post_id).first()


def get_all_user_posts(user_id=None):
    return ArtFlow.run(
        f"""
            MATCH (u:User)-[r:PUBLISHES]->(p:Post)
            WHERE (u.email = '{user_id}')
            RETURN p
            order by p.date_created desc 
        """
    )


def get_all_users_posts(users_list):
    posts = []
    for user in users_list:
        posts.extend(get_all_user_posts(user['email']))
    return posts


def update_post(post_id=None, new_text=None, new_media_link=None):
    post = load_postNode(post_id=post_id)
    if post:
        post['date_updated'] = str(datetime.today())
        post['text'] = new_text
        post['media'] = new_media_link
        ArtFlow.push(post)


def delete_post(post_id=None):
    post = load_postNode(post_id=post_id)
    if post:
        ArtFlow.delete(post)
        ArtFlow.push()
