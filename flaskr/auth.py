from flask import render_template

@app.route('/auth')
def auth():
    render_template('auth/auth.html')