import datetime
import os
from flask import Flask, render_template, request, redirect, url_for, flash
from py2neo import Graph
# from flaskr.pyArtFlow.inbox_actions import *
# from flaskr.pyArtFlow.user import UserNode
# from flaskr.pyArtFlow.user_actions import *
# from flaskr.pyArtFlow.config import ArtFlow, user
# from flaskr.user import routes


user_id = "karlschuller@yahoo.fr"
identity_token = "4d6c-a7f6-950b4c47938f"
# user = load_userNode(user_id)
# receiver_id = ArtFlow.nodes.match("User").all()[3]["email"]

def create_app():
    app = Flask(__name__)

    @app.route('/')
    def index():
        return render_template('index/index.html')

    @app.route('/auth')
    def auth():
        return render_template('auth/auth.html')

    @app.route('/dashboard')
    def dashboard():
        return render_template('dashboard/dashboard.html')

    @app.route('/dashboard2')
    def dashboard2():
        return render_template('dashboard/dashboard2.html')

    @app.route('/home')
    def home():
        return render_template('home/home.html')

    @app.route('/inbox')
    def inbox():
        return render_template('inbox/inbox.html')

    @app.route('/profil')
    def profil():
        return render_template('profil/profil.html')

    @app.route('/update_profil')
    def update_profil():
        return render_template('profil/update_profil.html')

    return app



# def keys():
#     emails = [friend_id.get('f')['email'] for friend_id in friends(user_id)]
#     return {email: str(hash(email)) for email in emails}
#
#
# key = keys()
#
#
# def create_app(test_config=None):
#     # create and configure the app
#     app = Flask(__name__, instance_relative_config=True)
#     app.config.from_mapping(
#         SECRET_KEY='dev',
#         DATABASE=os.path.join(app.instance_path, 'flaskr.sqlite'),
#     )
#
#     if test_config is None:
#         # load the instance config, if it exists, when not testing
#         app.config.from_pyfile('config.py', silent=True)
#     else:
#         # load the test config if passed in
#         app.config.from_mapping(test_config)
#
#     # ensure the instance folder exists
#     try:
#         os.makedirs(app.instance_path)
#     except OSError:
#         pass
#
#     # Routes
#     @app.route('/')
#     def index():
#         return render_template('index.html')
#
#     @app.route('/connexion', methods=['GET', 'POST'])
#     def home():
#         error = None
#         if request.method == "POST":
#             user = UserNode(
#                 identity_token=request.form['password'],
#                 first_name=request.form['firstname'],
#                 last_name=request.form['lastname'],
#                 user_name=request.form['username'],
#                 email=request.form['email'],
#                 phoneNo=request.form['phone'],
#                 country=request.form['country'],
#                 city=request.form['city'],
#                 birthday=request.form['birthday'],
#                 status=request.form['password'],
#                 gender=request.form['gender'],
#                 profession=request.form['profession'],
#                 website=request.form['website']
#             )
#             try:
#                 create_user(user)                               #Création du user dans la database
#                 return redirect(url_for('dashboard'))           #If created, then go to the fil_dactu template
#             except:
#                 error = "This user is already registered"
#
#         if request.method == 'GET' and request.args:
#             email = request.args.get('email', None)
#             id = request.args.get('password', None)
#             user = load_user(email, id)                         #Chargement du user depuis la database
#             if user is None:
#                 error = "Identifiants incorrects"
#             else:
#                 return redirect(url_for('dashboard'))           #if user loaded, go to the fil_dactu template
#
#         if error:
#             flash(error)
#         return render_template('home.html')
#
#     @app.route('/dashboard', methods=['GET', 'POST'])
#     def dashboard():
#         return render_template('home.html', user=user)
#
#
#     @app.route("/profil")
#     def Profil():
#         return render_template("Profil.html", user=user)
#
#     @app.route("/dashboard2")
#     def Dashboard():
#         return render_template("dashboard2.html", user=user)
#
#     @app.route("/Profile/Changer", methods=['GET', 'POST'])
#     def Changer():
#         return render_template("Changer.html", user=user)
#
#     # @app.route('/user/signup', methods=['POST'])
#     # def signup():
#     #     return User().signup()
#     #
#     # @app.route('/user/signout')
#     # def signout():
#     #     return User().signout()
#     #
#     # @app.route('/user/login', methods=['POST'])
#     # def login():
#     #     return User().login()
#     #
#     # @app.route('/Profil/Changer', methods=['POST'])
#     # def modifier():
#     #     return User().modifier()
#
#
#     @app.route('/inbox')
#     def inbox():
#         def conversations():
#             for contact, message, unread in list(load_inbox(user_id)):
#                 name = contact['first_name'] + " " + contact["last_name"]
#                 sms = message["message"]
#                 time = datetime_format(message['creation_datetime'])
#                 email = contact['email']
#                 try:
#                     assert len(sms) > 20
#                     sms = sms[:20] + "..."
#                 except:
#                     pass
#
#                 messages = load_conversation([user_id, contact["email"]])
#                 yield name, sms, time, unread, key[email], messages
#
#         def get_email(contact_key):
#             for k in key:
#                 if key[k] == contact_key:
#                     return k
#
#         return render_template(
#             "inbox.html",
#             user_id=user['email'],
#             conversations=conversations,
#             contacts=friends(user['email']),
#             send_message=send_message,
#             edit_conversation=edit_conversation,
#         )
#
#
#     @app.route('/inbox/<receiver>')
#     def edit(receiver):
#         print('Entrée')
#         for k in key:
#             if key[k] == receiver:
#                 friend_id = k
#                 break
#         print(friend_id)
#         edit_conversation(user_id, friend_id)
#
#         return None
#
#     return app